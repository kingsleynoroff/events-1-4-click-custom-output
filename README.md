# Basic click event 4

This is the same as task 3 but this time, the text that is output is going to come from a text box that we add to the page.

_Note:_ Styles are provided for you in master.css. Look in the file to find the approriate class names to use on your HTML elements.

### Example

[The example is here](https://events-1-4-click-custom-output.now.sh).

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Add a button and a text input into the HTML, make sure to add the appropriate class onto each element.
1. After the button add a *div* with the class name of "output".
1. Select the button, text input and div in JavaScript.
1. Add an event listener onto the button.
1. Create a callback function that is passed into the event listener.
1. When the callback is called, it should _add_ the text from the input element into the output. The value of the input can be retrieved by using the code *textInputElem.value*.