// select element from the DOM
const button = document.getElementById("btn");
const textbox = document.getElementById("textbox");
const output = document.getElementById("output");

// add event "click" event listner to the button.
// the function passed in will be invoked when the event happens
button.addEventListener("click", function() {
    // this code will be called when the button is clicked:

    // set the innerHTML of the output element to the value of the textbox
    output.innerText += textbox.value;
});
